# coding : utf-8

"""
...
Par Younnoussa Adam, Lallinec Corentin, Parrein Etienne
Licence Creative Commons CC BY-NC-SA   https://tinyurl.com/5n6uphyj
Version : 1.1
15/02/2022

"""


from csv import DictReader
from math import sqrt
# Importation nésséssaire au bon fonctionnement du programme

students = [{'Courage': 9, 'Ambition': 2, 'Intelligence': 8, 'Good': 9},
            {'Courage': 6, 'Ambition': 7, 'Intelligence': 9, 'Good': 7},
            {'Courage': 3, 'Ambition': 8, 'Intelligence': 6, 'Good': 3},
            {'Courage': 2, 'Ambition': 3, 'Intelligence': 7, 'Good': 8},
            {'Courage': 3, 'Ambition': 4, 'Intelligence': 8, 'Good': 8}]
# Constante des valeur du personnage

# Importation des deux bases de données en listes de dictionnaires
with open("Caracteristiques_des_persos.csv", mode="r", encoding="utf-8")as ct:
    stats_table = []
    csv_table = DictReader(ct, delimiter=";")
    for element in csv_table:
        stats_table.append(dict(element))
with open("Characters.csv", mode="r", encoding="utf-8")as ch:
    info_table = []
    csv_table_info = DictReader(ch, delimiter=";")
    for element in csv_table_info:
        info_table.append(element)

# Conversion des nombres de la liste en entiers
for i in range(len(stats_table)):
    for key, value in stats_table[i].items():
        if key != 'Name':
            stats_table[i][key] = int(stats_table[i][key])

# Création d'une info_table avec seulement les informations principales
simple_info_table = []
for element in info_table:
    objet = {}
    objet['Id'] = element['Id']
    objet['Name'] = element['Name']
    objet['Gender'] = element['Gender']
    objet['House'] = element['House']
    objet['Hair colour'] = element['Hair colour']
    objet['Eye colour'] = element['Eye colour']
    simple_info_table.append(objet)

# Réunion des deux listes de dictionnaires
short_list = {}
for people in stats_table:
    for info in simple_info_table:
        if people['Name'] == info['Name']:
            name = people['Name']
            info.update(people)
            short_list[name] = info
for person, stats in short_list.items():
    del(stats['Name'])


def maison(stats, neighbors=5):
    """
    Prend un profil d'élève et renvoie sa maison en fonction de ses kPPV
    ----------
    neighbors : Nombre k de voisins (5 par défaut)
    stats : Dictionnaire de 4 éléments sous la forme suivante
        'Courage' : ...
        'Ambition' : ...
        'Intelligence' : ...
        'Good' : ...
    Les valeurs doivent être des entiers compris entre 0 et 9

    Returns :
    -------
    """

    # Verification de la structure des arguments
    if type(neighbors) != int or neighbors < 1:
        print('Votre argument pour la fonction "maison" est incorrect')
        return None
    if len(stats) != 4 or type(stats) != dict:
        print('Votre argument pour la fonction "maison" est incorrect')
        return None
    for key, value in stats.items():
        if type(value) != int or type(key) != str:
            print('Votre argument pour la fonction "maison" est incorrect')
            return None
        if key not in 'CourageAmbitionIntelligenceGood':
            print('Votre argument pour la fonction "maison" est incorrect')
            return None

    # Coeur de la fonction, utilisation de la formule
    proximity_list = []
    for name, people in short_list.items():

        proximity = sqrt((people['Courage'] - stats['Courage'])**2 +
                         (people['Ambition'] - stats['Ambition'])**2 +
                         (people['Intelligence'] - stats['Intelligence'])**2 +
                         (people['Good'] - stats['Good'])**2)
        proximity_list.append([name, people['House'], round(proximity, 2)])
    proximity_list = sorted(proximity_list, key=lambda x: x[2])

    # Attribution de la maison selon les kPPV
    del(proximity_list[neighbors:])
    occurency = {'Gryffindor': 0,
                 'Slytherin': 0,
                 'Ravenclaw': 0,
                 'Hufflepuff': 0}
    for people in proximity_list:
        occurency[people[1]] += 1
    occurency_list = [[key, value] for key, value in occurency.items()]
    occurency_list.sort(key=lambda x: x[1], reverse=True)
    return occurency_list[0][0], proximity_list


# début de l'IHM
input('Bienvenue, tapez entrée pour voire les maisons affectés aux différents\
 profils tests')
for i in range(len(students)):
    profile = maison(students[i])
    stats_values = [x for x in students[i].values()]
    print(f'\n{i + 1}/  La maison du profil {stats_values} est \
{profile[0]}:')
    for i in range(len(profile[1])):
        if i == 0:
            print(f'      Le plus proche voisin est {profile[1][i][0]}\
, maison {profile[1][i][1]}.')
        else:
            print(f'      Le {i + 1}ème plus proche voisin est \
{profile[1][i][0]}, maison {profile[1][i][1]}.')

# Affichage des premières listes

answer = '1'
neighbors = 'None'
while answer in '123':

    answer = input('Voulez vous rédiger un profil précis (1), changer la \
proximité k pour les profils tests (2), avoir une vision complète des données\
 (3), ou bien quitter le programme (4).\n')

    if answer == '1':  # Création d'un profil
        made_profile = {'Courage': None, 'Ambition': None,
                        'Intelligence': None, 'Good': None}
        for competence in made_profile.keys():
            made_profile[competence] = int(input(f'Quel est la valeur de la'
                                                 f' caractéristique '
                                                 f'{competence} du profil'
                                                 f' créé ?\n'))
        while neighbors not in '123456789' or len(neighbors) != 1:
            neighbors = input('Quelle est la valeur de k (max 9) ? ')
        neighbors = int(neighbors)
        profile = maison(made_profile, neighbors)
        stats_values = [x for x in made_profile.values()]
        print(f'\n La maison du profil {stats_values} est \
{profile[0]}:')  # Affichage des résultats du profil
        for i in range(len(profile[1])):
            if i == 0:
                print(f'   Le plus proche voisin est {profile[1][i][0]}\
, maison {profile[1][i][1]}.')
            else:
                print(f'   Le {i + 1}ème plus proche voisin est \
{profile[1][i][0]}, maison {profile[1][i][1]}.')

    elif answer == '2':  # Modification de k pour les profils tests
        while neighbors not in '123456789' or len(neighbors) != 1:
            neighbors = input('Quelle est la valeur de k ? (max 9)')
        neighbors = int(neighbors)

        for i in range(len(students)):
            profile = maison(students[i], neighbors)
            stats_values = [x for x in students[i].values()]
            print(f'\n{i + 1}/  La maison du profil {stats_values} est \
{profile[0]}:')
            for i in range(len(profile[1])):
                if i == 0:
                    print(f'      Le plus proche voisin est {profile[1][i][0]}\
, maison {profile[1][i][1]}.')
                else:
                    print(f'      Le {i + 1}ème plus proche voisin est \
{profile[1][i][0]}, maison {profile[1][i][1]}.')

# Début de l'affichage complet des élèves
    elif answer == '3':
        counter = 0
        for pupile in students:
            counter += 1
            profile = maison(pupile)
            stat = [x for x in pupile.values()]
            print(f'\n--------------------------------- | Courage | Ambition |'
                  f' Intelligence | Good |    | {"House":^15} | Proximity |\n')
            print(f'tested profil (n°{counter})             : | {stat[0]:^7} |'
                  f' {stat[1]:^8} | {stat[2]:^12} | {stat[3]:^4} |    | '
                  f'{profile[0]:^15} | {"*":^9} |')
            print("\n-----kPPV-----")
            for y in range(len(profile[1])):
                print(f'{profile[1][y][0]:<32}: |\
 {short_list[profile[1][y][0]]["Courage"]:^7} |\
 {short_list[profile[1][y][0]]["Ambition"]:^8} |\
 {short_list[profile[1][y][0]]["Intelligence"]:^12} |\
 {short_list[profile[1][y][0]]["Good"]:^4} |\
    | {profile[1][y][1]:^15} | {profile[1][y][2]:^9} |')
            print("--------------")
    else:
        print('Merci pour votre temps !!!')
        
    neighbors = 'None'

# coding : utf-8

"""
...
Par Younnoussa Adam, Lallinec Corentin, Parrein Etienne 
Licence Creative Commons CC BY-NC-SA   https://tinyurl.com/5n6uphyj
Version : 1.1
15/02/2022

"""


from csv import DictReader

students = [{'Courage' : 9, 'Ambition' : 2, 'Intelligence' : 8, 'Good' : 9},
            {'Courage' : 6, 'Ambition' : 7, 'Intelligence' : 9, 'Good' : 7},
            {'Courage' : 3, 'Ambition' : 8, 'Intelligence' : 6, 'Good' : 3},
            {'Courage' : 2, 'Ambition' : 3, 'Intelligence' : 7, 'Good' : 8},
            {'Courage' : 3, 'Ambition' : 4, 'Intelligence' : 8, 'Good' : 8}]

# Importation des deux bases de données en listes de dictionnaires
with open("Caracteristiques_des_persos.csv", mode="r", encoding="utf-8")as ct:
    stats_table = []
    csv_table = DictReader(ct, delimiter=";")
    for element in csv_table:
        stats_table.append(element)       
with open("Characters.csv", mode="r", encoding="utf-8")as ch:
    info_table = []
    csv_table_info = DictReader(ch, delimiter=";")
    for element in csv_table_info:
        info_table.append(element)
        
# Création d'une info_table avec seulement les informations principales
simple_info_table = []
for element in info_table:
    objet = {}
    objet['Id'] = element['Id']
    objet['Name'] = element['Name']
    objet['Gender'] = element['Gender']
    objet['House'] = element['House']
    objet['Hair colour'] = element['Hair colour']
    objet['Eye colour'] = element['Eye colour']
    simple_info_table.append(objet)

# Réunion des deux listes de dictionnaires
short_list = {}
for people in stats_table:
    for info in simple_info_table:
        if people['Name'] == info['Name']:
            name = people['Name']
            info.update(people)
            short_list[name] = info
for person,stats in short_list.items():
    del(stats['Name'])

print(short_list)


